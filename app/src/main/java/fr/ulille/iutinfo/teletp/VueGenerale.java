package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.util.ArrayList;

public class VueGenerale extends Fragment {
    private String dist;
    // TODO Q1
    private String salle;
    private String poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel suiv;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        Context c = getContext();
        this.poste = "";
        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.salle = this.DISTANCIEL;

        // TODO Q2.c
        suiv = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner salle = getActivity().findViewById(R.id.spSalle);
        SpinnerAdapter adapter = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles, android.R.layout.simple_spinner_item);
        salle.setAdapter(adapter);
        Spinner poste = getActivity().findViewById(R.id.spPoste);
        SpinnerAdapter adapter2 = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes, android.R.layout.simple_spinner_item);
        poste.setAdapter(adapter2);


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText tmp = getActivity().findViewById(R.id.tvLogin);
            suiv.setUsername(tmp.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        salle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView adapterView, View view, int i, long l) {
                update();
            }
            public void onNothingSelected(AdapterView adapterView) { return; }
        });
        // TODO Q9
    }

    // TODO Q5.a
    void update() {
        Spinner salle = getActivity().findViewById(R.id.spSalle);
        Spinner poste = getActivity().findViewById(R.id.spPoste);
        if(salle.getSelectedItem().toString() == "Distanciel"){
            poste.setVisibility(View.GONE);
            poste.setEnabled(false);
            suiv.setLocalisation("Distanciel");
        }else{
            poste.setVisibility(View.VISIBLE);
            poste.setEnabled(true);
            suiv.setLocalisation(salle.getSelectedItem().toString() + " : " + poste.getSelectedItem().toString());
        }
    }
    // TODO Q9
}